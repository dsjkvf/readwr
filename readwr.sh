#!/bin/bash

# define the exit routine...
trap 'clean_exit' EXIT TERM
trap 'clean_exit' HUP
trap 'clean_exit' INT
# ... which will clean up the temp file
clean_exit() {
    rm -f $text;
}

# define plugins for special cases
if echo "$1" | grep -iq 'github.com.*commit'
then
    $(dirname $(readlink $0))/readwr-github-commit.sh $1 | less -+F
    exit
fi

# on macOS, capture the current terminal
if uname | grep -iq darwin
then
    term=$(osascript -e "tell application \"System events\" to set p to first process whose frontmost is true" | cut -d' ' -f3)
fi
# init the temp file
text=$(mktemp $TMPDIR/read.XXXXXX)
# parse the contents of the provided URL to the temp file
read.py "$1" > "$text"
echo '<br><a href="'$1'">PERMALINK</a>' >> "$text"

# # handle special cases
# case $1 in
#     https://www.reuters.com*|https://reuters.com*)
#         perl -pi -e 's/<div class=.registration-prompt__text-content.*?div>//g;' "$text"
#         ;;
# esac

# if successful, read the contents of the temp file with the terminal browser
if [ $? -eq 0 ]
then
    # read
    w3m -o confirm_qq=false -T text/html < "$text"
# otherwise...
else
    # ...on macOS, open the provided URL in the system browser and return to the current terminal
    if uname | grep -iq darwin
    then
        open "$1"
        sleep 1
        osascript -e 'tell application "'$term'" to activate'
    # otherwise inform about the failure
    else
        printf "\033[1;31mERROR:\033[0m Failed to readify the original page at:\n"
        printf "\033[1;36m              $1\033[0m\n"
        exit 1
    fi
fi
