#!/bin/bash

# define the exit routine...
trap 'clean_exit' EXIT TERM
trap 'clean_exit' HUP
trap 'clean_exit' INT
# ... which will clean up the temp file
clean_exit() {
    rm -f $text;
}

# define sed application
SED=$(which gsed || which sed)

# init the temp file
text=$(mktemp $TMPDIR/read.XXXXXX)
# parse the contents of the provided URL to the temp file
read.py "$1" > "$text"

# if successful, read the contents of the temp file with the terminal browser
if [ $? -eq 0 ]
then
    $SED -i "s/data-code-marker=\"+\">/>† /g" $text
    $SED -i "s/data-code-marker=\"-\">/>ˇ /g" $text
    # read
    w3m -dump -cols 999 -o confirm_qq=false -T text/html < "$text" | \
        perl -pe 's/†(.*)/[32;1m+$1[0m/' | \
        perl -pe 's/ˇ(.*)/[0;31m-$1[0m/' | \
        $SED '/^Toggle all file notes/,$d'

# otherwise inform about the failure
else
    printf "\033[1;31mERROR:\033[0m Failed to readify the original page at:\n"
    printf "\033[1;36m              $1\033[0m\n"
fi
