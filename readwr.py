#!/usr/bin/env python3

import sys
import requests
# https://github.com/buriy/python-readability/
from readability import Document

try:
    url = sys.argv[1:2][0]
except:
    print("USAGE: read <URL>")
    sys.exit()
response = requests.get(url)
# https://stackoverflow.com/a/59992216/1068046
doc = Document(response.content)
if doc.summary() == '<html><body></body></html>':
    sys.exit(1)
else:
    print(doc.summary())
